﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Control.DAL.Entities;
using Control.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Control.DAL.Repositories
{
    public class InstitutionRepository : Repository<Institution>, IInstitutionRepository
    {
        public InstitutionRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Institutions;
        }
        public IEnumerable<Institution> GetAllWithAuthorsAndComments()
        {
            return entities
                .Include(e => e.User)
                .Include(e => e.Comments)
                .ThenInclude(c => c.User)
                .ToList();
        }
        public Institution GetByIdWithAuthorsAndComments(int id)
        {
            return entities
                .Include(e => e.User)
                .Include(e => e.Comments)
                .ThenInclude(c => c.User)
                .FirstOrDefault(c => c.Id == id);
        }
    }
}
