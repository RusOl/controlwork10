﻿using System;
using System.Collections.Generic;
using System.Text;
using Control.DAL.Entities;

namespace Control.DAL.Repositories.Contracts
{
    public interface ICommentRepository : IRepository<Comment>
    {
        Comment GetByIdCommentWithAll(int id);
        IEnumerable<Comment> GetAllCommentWithAll();
    }
}
