﻿using System;
using System.Collections.Generic;
using System.Text;
using Control.DAL.Entities;

namespace Control.DAL.Repositories.Contracts
{
    public interface IInstitutionRepository : IRepository<Institution>
    {
        IEnumerable<Institution> GetAllWithAuthorsAndComments();
        Institution GetByIdWithAuthorsAndComments(int id);
    }
}
