﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Control.DAL.Entities.Contracts;

namespace Control.DAL.Repositories.Contracts
{
    public interface IRepository<T> where T : class, IEntity
    {
        Task<T> CreateAsync(T entity);
        T Create(T entity);

        T GetById(int id);

        IEnumerable<T> GetAll();

        T Update(T entity);

        void Remove(T entity);
    }
}
