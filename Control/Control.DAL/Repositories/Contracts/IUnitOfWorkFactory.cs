﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Control.DAL.Repositories.Contracts
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
