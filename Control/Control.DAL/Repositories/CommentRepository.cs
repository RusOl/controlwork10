﻿using System.Collections.Generic;
using System.Linq;
using Control.DAL.Entities;
using Control.DAL.Repositories.Contracts;
using Microsoft.EntityFrameworkCore;

namespace Control.DAL.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Comments;
        }
        public Comment GetByIdCommentWithAll(int id)
        {
            return GetAllCommentWithAll().FirstOrDefault(b => b.Id == id);
        }
        public IEnumerable<Comment> GetAllCommentWithAll()
        {
            return entities
                .Include(b => b.Institution)
                .Include(b => b.User)
                .ToList();
        }

       
    }
}
