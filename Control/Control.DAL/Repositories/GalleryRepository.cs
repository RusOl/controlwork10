﻿using System;
using System.Collections.Generic;
using System.Text;
using Control.DAL.Entities;
using Control.DAL.Repositories.Contracts;

namespace Control.DAL.Repositories
{
    public class GalleryRepository : Repository<Gallery>, IGalleryRepository
    {
        public GalleryRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Galleries;
        }
    }
}
