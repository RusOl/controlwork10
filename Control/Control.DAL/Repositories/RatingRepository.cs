﻿using System;
using System.Collections.Generic;
using System.Text;
using Control.DAL.Entities;
using Control.DAL.Repositories.Contracts;

namespace Control.DAL.Repositories
{
    public class RatingRepository : Repository<Rating>, IRatingRepository
    {
        public RatingRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Ratings;
        }
    }
}
