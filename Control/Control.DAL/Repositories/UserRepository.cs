﻿using System;
using System.Collections.Generic;
using System.Text;
using Control.DAL.Entities;
using Control.DAL.Repositories.Contracts;

namespace Control.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {
            entities = context.Users;
        }
    }
}
