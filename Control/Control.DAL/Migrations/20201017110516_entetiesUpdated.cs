﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Control.DAL.Migrations
{
    public partial class entetiesUpdated : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Image",
                table: "Galleries");

            migrationBuilder.AddColumn<string>(
                name: "PathFile",
                table: "Galleries",
                maxLength: 2147483647,
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_Ratings_Id",
                table: "Ratings",
                column: "Id",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_Ratings_Id",
                table: "Ratings");

            migrationBuilder.DropColumn(
                name: "PathFile",
                table: "Galleries");

            migrationBuilder.AddColumn<byte[]>(
                name: "Image",
                table: "Galleries",
                type: "varbinary(max)",
                nullable: true);
        }
    }
}
