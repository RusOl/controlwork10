﻿using System;
using System.Collections.Generic;
using System.Text;
using Control.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Control.DAL.EntitiesConfiguration
{
    public class GalleryConfiguration : BaseEntityConfiguration<Gallery>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Gallery> builder)
        {
            builder
                .Property(p => p.PathFile)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
        }
        protected override void ConfigureForeignKeys(EntityTypeBuilder<Gallery> builder)
        {
            builder
                .HasOne(b => b.User)
                .WithMany(b => b.Galleries)
                .HasForeignKey(b => b.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .HasOne(r => r.Institution)
                .WithMany(r => r.Galleries)
                .HasForeignKey(r => r.InstitutionId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

        }
    }
}
