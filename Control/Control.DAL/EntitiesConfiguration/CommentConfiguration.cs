﻿using Control.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Control.DAL.EntitiesConfiguration
{
    public class CommentConfiguration : BaseEntityConfiguration<Comment>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Comment> builder)
        {
            builder
                .Property(b => b.Content)
                .HasMaxLength(1000)
                .IsRequired();

            builder
                .Property(b => b.CreatedOn)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Comment> builder)
        {
            builder
                .HasOne(b => b.User)
                .WithMany(b => b.Comments)
                .HasForeignKey(b => b.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
            builder
                .HasOne(b => b.Institution)
                .WithMany(b => b.Comments)
                .HasForeignKey(b => b.InstitutionId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
