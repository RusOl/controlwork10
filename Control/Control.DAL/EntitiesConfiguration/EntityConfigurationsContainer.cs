﻿using Control.DAL.Entities;
using Control.DAL.EntitiesConfiguration.Contracts;

namespace Control.DAL.EntitiesConfiguration
{
    public class EntityConfigurationsContainer : IEntityConfigurationsContainer
    {
        public IEntityConfiguration<Institution> InstitutionConfiguration { get; }
        public IEntityConfiguration<Rating> RatingConfiguration { get; }
        public IEntityConfiguration<User> UserConfiguration { get; }
        public IEntityConfiguration<Comment> CommentConfiguration { get; }
        public IEntityConfiguration<Gallery> GalleryConfiguration { get; }
        

        public EntityConfigurationsContainer()
        {
            InstitutionConfiguration = new InstitutionConfiguration();
            RatingConfiguration = new RatingConfiguration();
            UserConfiguration = new UserConfiguration();
            CommentConfiguration = new CommentConfiguration();
            GalleryConfiguration = new GalleryConfiguration();
        }
    }
}
