﻿using Control.DAL.Entities;

namespace Control.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfigurationsContainer
    {
        IEntityConfiguration<User> UserConfiguration { get; }
        IEntityConfiguration<Institution> InstitutionConfiguration { get; }
        IEntityConfiguration<Comment> CommentConfiguration { get; }
        IEntityConfiguration<Gallery> GalleryConfiguration { get; }
        IEntityConfiguration<Rating> RatingConfiguration { get; }
    }
}
