﻿using System;
using Control.DAL.Entities.Contracts;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Control.DAL.EntitiesConfiguration.Contracts
{
    public interface IEntityConfiguration<T> where T : class, IEntity
    {
        Action<EntityTypeBuilder<T>> ProvideConfigurationAction();
    }
}
