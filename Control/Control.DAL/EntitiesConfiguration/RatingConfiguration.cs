﻿using Control.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Control.DAL.EntitiesConfiguration
{
    public class RatingConfiguration : BaseEntityConfiguration<Rating>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Rating> builder)
        {
            builder
                .HasIndex(p => p.Id)
                .IsUnique();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Rating> builder)
        {
            builder
                .HasOne(b => b.User)
                .WithMany(b => b.Ratings)
                .HasForeignKey(b => b.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasOne(b => b.Institution)
                .WithMany(b => b.Ratings)
                .HasForeignKey(b => b.InstitutionId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();
        }
    }
}
