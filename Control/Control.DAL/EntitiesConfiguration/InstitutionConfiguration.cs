﻿using Control.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Control.DAL.EntitiesConfiguration
{
    public class InstitutionConfiguration : BaseEntityConfiguration<Institution>
    {
        protected override void ConfigureProperties(EntityTypeBuilder<Institution> builder)
        {
            builder
                .Property(b => b.Name)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .Property(b => b.Address)
                .HasMaxLength(100)
                .IsRequired();
            builder
                .Property(b => b.Description)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .Property(b => b.Image)
                .HasMaxLength(int.MaxValue)
                .IsRequired();
            builder
                .Property(b => b.DatePublished)
                .IsRequired();
        }

        protected override void ConfigureForeignKeys(EntityTypeBuilder<Institution> builder)
        {
            builder
                .HasOne(b => b.User)
                .WithMany(b => b.Institutions)
                .HasForeignKey(b => b.UserId)
                .OnDelete(DeleteBehavior.Restrict)
                .IsRequired();

            builder
                .HasMany(b => b.Comments)
                .WithOne(b => b.Institution)
                .HasForeignKey(b => b.InstitutionId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(b => b.Galleries)
                .WithOne(b => b.Institution)
                .HasForeignKey(b => b.UserId)
                .OnDelete(DeleteBehavior.Restrict);

            builder
                .HasMany(b => b.Ratings)
                .WithOne(b => b.Institution)
                .HasForeignKey(b => b.InstitutionId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
