﻿using Control.DAL.Entities;
using Control.DAL.EntitiesConfiguration.Contracts;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Control.DAL
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, int>
    {
        private readonly IEntityConfigurationsContainer _entityConfigurationsContainer;

        public DbSet<Comment> Comments { get; set; }
        public DbSet<Institution> Institutions { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Gallery> Galleries { get; set; }
        

        public ApplicationDbContext(
            DbContextOptions options,
            IEntityConfigurationsContainer entityConfigurationsContainer) : base(options)
        {
            _entityConfigurationsContainer = entityConfigurationsContainer;
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity(_entityConfigurationsContainer.InstitutionConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.GalleryConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.CommentConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.UserConfiguration.ProvideConfigurationAction());
            builder.Entity(_entityConfigurationsContainer.RatingConfiguration.ProvideConfigurationAction());
        }
    }
}
