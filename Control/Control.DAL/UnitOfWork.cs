﻿using System;
using Control.DAL.Repositories;
using Control.DAL.Repositories.Contracts;

namespace Control.DAL
{
    public class UnitOfWork : IDisposable
    {
        private readonly ApplicationDbContext _context;
        private bool _disposed;

        public IInstitutionRepository Institutions { get; set; }
        public IGalleryRepository Galleries { get; set; }
        public IUserRepository Users { get; set; }
        public ICommentRepository Comments { get; set; }
        public IRatingRepository Ratings { get; set; }

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;

            Institutions = new InstitutionRepository(context);
            Galleries = new GalleryRepository(context);
            Users = new UserRepository(context);
            Comments = new CommentRepository(context);
            Ratings = new RatingRepository(context);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        private void Dispose(bool disposing)
        {
            if (_disposed)
                return;

            if (disposing)
                _context.Dispose();

            _disposed = true;
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
        #endregion
    }
}
