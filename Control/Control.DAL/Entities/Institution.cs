﻿using System;
using System.Collections.Generic;
using Control.DAL.Entities.Contracts;

namespace Control.DAL.Entities
{
    public  class Institution : IEntity
    {
        public int Id { get; set; }
        public DateTime DatePublished { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public byte[] Image { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public ICollection<Comment> Comments { get; set; }
        public ICollection<Gallery> Galleries { get; set; }
        public ICollection<Rating> Ratings { get; set; }
    }
}
