﻿using Control.DAL.Entities.Contracts;
using Microsoft.AspNetCore.Identity;

namespace Control.DAL.Entities
{
    public class Role : IdentityRole<int>, IEntity
    {
    }
}
