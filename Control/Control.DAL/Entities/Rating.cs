﻿
using Control.DAL.Entities.Contracts;

namespace Control.DAL.Entities
{
    public class Rating : IEntity
    {
        public int Id { get; set; }
        public int Rate { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int InstitutionId { get; set; }
        public Institution Institution { get; set; }
    }
}
