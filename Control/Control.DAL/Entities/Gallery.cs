﻿
using Control.DAL.Entities.Contracts;

namespace Control.DAL.Entities
{
    public class Gallery : IEntity
    {
        public int Id { get; set; }

        public int UserId { get; set; }

        public User User { get; set; }

        public int InstitutionId { get; set; }

        public Institution Institution { get; set; }

        public string PathFile { get; set; }
    }
}
