﻿using System;
using System.Collections.Generic;
using System.Text;
using Control.DAL.Entities.Contracts;

namespace Control.DAL.Entities
{
    public class Comment : IEntity
    {
        public int Id { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Content { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public int InstitutionId { get; set; }
        public Institution Institution { get; set; }
    }
}
