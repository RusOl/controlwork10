﻿using System.Collections.Generic;
using Control.DAL.Entities.Contracts;
using Microsoft.AspNetCore.Identity;

namespace Control.DAL.Entities
{
    public class User : IdentityUser<int>, IEntity
    {
        public override int Id { get; set; }
        public ICollection<Comment> Comments { get; set; }

        public ICollection<Gallery> Galleries { get; set; }

        public ICollection<Institution> Institutions { get; set; }

        public ICollection<Rating> Ratings { get; set; }
    }
}
