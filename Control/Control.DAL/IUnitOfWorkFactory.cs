﻿
namespace Control.DAL
{
    public interface IUnitOfWorkFactory
    {
        UnitOfWork Create();
    }
}
