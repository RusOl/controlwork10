﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Control.DAL;
using Control.DAL.Entities;
using Control.Models;
using Control.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Control.Controllers
{
    public class InstitutionController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly IInstitutionService _inService;
       public InstitutionController(UserManager<User> userManager, IInstitutionService inService, IUnitOfWorkFactory unitOfWork)
        {
            _userManager = userManager;
            _inService = inService;
            }
        public IActionResult Index(InstitutionIndexModel model, string search)
        {
            try
            {
                model.SearchKey = search;
                var recordModels = _inService.GetAllAds(model);
                model.InstitutionModels = recordModels;
                return View(model);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
        [Authorize]
        public IActionResult Create()
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> CreateInstitution(InstitutionCreateModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);

                _inService.CreateAd(model, currentUser.Id);

                return RedirectToAction("Index");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        [Authorize]
        [HttpGet]
        public IActionResult Details(int recordId)
        {
            var recordModel = _inService.GetAdById(recordId);
            recordModel.NumbersSelect = GenerateNumbersSelect(5);
            return View(recordModel);
        }

        private SelectList GenerateNumbersSelect(int number, int? selectedNumber = null)
        {
            return new SelectList(Enumerable.Range(1, number).Select(x => new SelectListItem()
            {
                Text = x.ToString(),
                Value = x.ToString()
            }), "Value", "Text", selectedNumber);
        }

        [HttpGet]
        public IActionResult AddComment(int instId)
        {
            var model = _inService.GetAddCommentModel(instId);
                
            return View(model);
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> AddComment(AddCommentModel model)
        {
            try
            {
                var curUser = await _userManager.GetUserAsync(User);
                _inService.AddComment(model, curUser);

                return RedirectToAction("Index", "Institution"); ;
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
        [Authorize]
        [HttpPost]
        public async Task<IActionResult> DownoloadPictures(AddGalleryModel model)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                _inService.DownoloadPictures(model, currentUser.Id);
                return RedirectToAction("Index", "Institution");
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
        
        [HttpPost]
        public async Task<IActionResult> RateAjax(int rating, int recordId)
        {
            try
            {
                User currentUser = await _userManager.GetUserAsync(User);
                var current = currentUser.Id;
                var recordModel = _inService.AddRate(recordId, rating, current);

                return Ok(recordModel);
            }
            catch (Exception e)
            {
                return StatusCode(500, e.Message);
            }
        }
    }
}
