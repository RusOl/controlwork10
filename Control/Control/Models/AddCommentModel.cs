﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Control.Models
{
    public class AddCommentModel
    {
        [Display(Name = "Комментарий")]
        public string Comment { get; set; }


        [Display(Name = "ID заведения")]
        public int instId { get; set; }
    }
}
