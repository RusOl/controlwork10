﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Control.Models
{
    public class GalleryModel
    {
        public int Id { get; set; }

        [Required]
        public int InstitutionId { get; set; }
        [Required]
        public int UserId { get; set; }
        [Required]
        public IFormFileCollection GalleriesCollection { get; set; }
    }
}
