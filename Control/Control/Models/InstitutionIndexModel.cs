﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Control.Models
{
    public class InstitutionIndexModel
    {
        public string Author { get; set; }
        public string SearchKey { get; set; }
        public List<InstitutionModel> InstitutionModels { get; set; }
        public int? Page { get; set; }
        public PagingModel PagingModel { get; set; }
    }
}
