﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Control.DAL.Entities;

namespace Control.Models
{
    public class RatingModel
    {
        public int Id { get; set; }
        public int Rate { get; set; }
    }
}
