﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Control.Models
{
    public class CommentModel
    {
        public int Id { get; set; }

        [Display(Name = "Дата публикации")]
        public DateTime CreatedOn { get; set; }

        [Display(Name = "Автор")]
        public string User { get; set; }
        public int UserId { get; set; }
        [Display(Name = "Содержание")]
        public string Content { get; set; }
        public string Institution { get; set; }
        public int InstitutionId { get; set; }
    }
}
