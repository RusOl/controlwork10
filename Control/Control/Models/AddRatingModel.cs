﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Control.Models
{
    public class AddRatingModel
    {
        public int Rate { get; set; }
    }
}
