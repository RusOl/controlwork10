﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Control.DAL.Entities;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Control.Models
{
    public class InstitutionModel
    {
        public int Id { get; set; }
        [Display(Name = "Дата публикации")]
        public DateTime DatePublished { get; set; }
        [Display(Name= "Название")]
        public string Name { get; set; }
        [Display(Name = "Адресс")]
        public string Address { get; set; }
        [Display(Name = "Описание")]
        public string Description { get; set; }
        public byte[] Image { get; set; }
        public string ImagePath { get; set; }
        public double Rating { get; set; }
        public SelectList NumbersSelect { get; set; }
        public List<Comment> Comments { get; set; }
        public List<CommentModel> CommentModels { get; set; }
        public Comment Comment { get; set; }
        public List<Rating> Ratings { get; set; }
        public List<Gallery> Galleries { get; set; }
    }
}
