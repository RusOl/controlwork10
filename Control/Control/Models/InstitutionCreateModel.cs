﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;

namespace Control.Models
{
    public class InstitutionCreateModel
    {
        [Required]
        [Display(Name = "Название заведения ")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Описание")]
        public string Description { get; set; }
        [Required]
        [Display(Name = "Адресс ")]
        public string Address { get; set; }
        [Required]
        [Display(Name = "Изображение")]
        public IFormFile Image { get; set; }
    }
}
