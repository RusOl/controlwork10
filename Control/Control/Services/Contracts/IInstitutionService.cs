﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Control.DAL.Entities;
using Control.Models;
using Microsoft.AspNetCore.Http;

namespace Control.Services.Contracts
{
    public interface IInstitutionService
    {
        public void AddComment(AddCommentModel model,User curUser);
        AddCommentModel GetAddCommentModel(int instId);
        List<InstitutionModel> GetAllAds(InstitutionIndexModel model);
        InstitutionModel GetAdById(int recordId);
        void CreateAd(InstitutionCreateModel model, int currentUserId);
        RatingModel AddRate(int recordId, int rate, int userId);
        void DownoloadPictures(AddGalleryModel model, int userId);
    }
}
