﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AutoMapper;
using Control.DAL;
using Control.DAL.Entities;
using Control.Models;
using Control.Services.Contracts;
using Microsoft.AspNetCore.Http;

namespace Control.Services
{
    public class InstitutionService : IInstitutionService
    {
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        private readonly IFileSaver _fileSaver;
        public InstitutionService(IUnitOfWorkFactory unitOfWorkFactory, IFileSaver fileSaver)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _fileSaver = fileSaver;
        }
        public List<InstitutionModel> GetAllAds(InstitutionIndexModel model)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var records = unitOfWork.Institutions.GetAllWithAuthorsAndComments();
                records = records
                    .BySearchKey(model.SearchKey)
                    .ByAuthorName(model.Author);
                int pageSize = 4;
                int count = records.Count();
                int page = model.Page.HasValue ? model.Page.Value : 1;
                records = records.Skip((page - 1) * pageSize).Take(pageSize);
                model.PagingModel = new PagingModel(count, page, pageSize);
                model.Page = page;

                var models = Mapper.Map<List<InstitutionModel>>(records.ToList());

                return models;
            }
        }

        public AddCommentModel GetAddCommentModel(int instId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var addCreateModel = new AddCommentModel()
                {
                    instId = instId
                };

                return addCreateModel;
            }
        }
        public void DownoloadPictures(AddGalleryModel model, int userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {

                foreach (var gall in model.GalleriesCollection)
                {
                    var path = DownoloadGallery(gall);
                    var gallery = new Gallery()
                    {
                        UserId = userId,
                        InstitutionId = model.InstitutionId,
                        PathFile = path
                    };
                    unitOfWork.Galleries.Create(gallery);
                }
            }
        }
        private string DownoloadGallery(IFormFile image)
        {
            string fileName = $"{Guid.NewGuid()}_{image.FileName}";
            string path = @"wwwroot";
            string secpath = @"uploads\images";
            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            if (!directoryInfo.Exists)
            {
                directoryInfo.Create();
            }
            string filePath = directoryInfo.CreateSubdirectory(secpath).ToString();
            filePath = $@"wwwroot/uploads/images/{fileName}";
            using (var fileStream = System.IO.File.Create(filePath))
            {
                image.CopyTo(fileStream);
            }
            return $@"/uploads/images/{fileName}";
        }
        public void AddComment(AddCommentModel model, User userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var comment = Mapper.Map<Comment>(model);

                comment.UserId = userId.Id;
                comment.InstitutionId = model.instId;

                comment.CreatedOn = DateTime.Now;

                comment.Content = model.Comment;

                unitOfWork.Comments.Create(comment);
            }
        }
        public InstitutionModel GetAdById(int recordId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var ratings = unitOfWork.Ratings.GetAll().Where(r => r.InstitutionId == recordId).ToList();
                var qtyRatings = ratings.Count;
                var sumRatings = ratings.Sum(r => r.Rate);
                double averegeRating = Math.Round(((double) sumRatings / qtyRatings), 1);
                var ad = unitOfWork.Institutions.GetByIdWithAuthorsAndComments(recordId);
                var gallery = unitOfWork.Galleries.GetAll().ToList();
                ad.Galleries = gallery;
                var institution = Mapper.Map<InstitutionModel>(ad);
                institution.Rating = averegeRating;
                return institution;
            }
        }

        public void CreateAd(InstitutionCreateModel model, int currentUserId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var record = Mapper.Map<Institution>(model);
                record.UserId = currentUserId;

                _fileSaver.SaveFile(record, model.Image);

                unitOfWork.Institutions.Create(record);
            }
        }
        public RatingModel AddRate(int recordId, int rate, int userId)
        {
            using (var unitOfWork = _unitOfWorkFactory.Create())
            {
                var rating = new Rating()
                {
                    InstitutionId = recordId,
                    Rate = rate,
                    UserId = userId,
                };

                unitOfWork.Ratings.Create(rating);
                return Mapper.Map<RatingModel>(rating);
            }

        }
    }
}
