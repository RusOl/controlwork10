﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Control.DAL.Entities;
using Microsoft.AspNetCore.Http;

namespace Control.Services
{
    public interface IFileSaver
    {
        void SaveFile(Institution record, IFormFile formFile);
    }
    public class DbFilesSaver : IFileSaver
    {
        public void SaveFile(Institution record, IFormFile formFile)
        {
            using (var binaryReader = new BinaryReader(formFile.OpenReadStream()))
            {
                record.Image = binaryReader.ReadBytes((int)formFile.Length);
            }
        }
    }
}
