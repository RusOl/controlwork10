﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Control.DAL.Entities;

namespace Control.Services
{
    public static class InstitutionServiceExtensions
    {
        public static IEnumerable<Institution> BySearchKey(this IEnumerable<Institution> ads, string searchKey)
        {
            if (!string.IsNullOrWhiteSpace(searchKey))
                ads = ads.Where(r => r.Name.ToLower().Contains(searchKey) || r.Name.ToLower().Contains(searchKey));

            return ads;
        }

        public static IEnumerable<Institution> ByAuthorName(this IEnumerable<Institution> ads, string authorName)
        {
            if (!string.IsNullOrWhiteSpace(authorName))
                ads = ads.Where(r => r.User.UserName.Contains(authorName));

            return ads;
        }

    }
}
