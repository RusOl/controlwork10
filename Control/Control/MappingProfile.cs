﻿using System;
using AutoMapper;
using Control.DAL.Entities;
using Control.Models;

namespace Control
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateModelMap<Rating, RatingModel>();
            CreateModelMap<Rating, AddRatingModel>();
            CreateModelMap<Institution, InstitutionModel>();
            CreateModelMap<Institution, InstitutionCreateModel>();
            CreateModelMap<Comment, CommentModel>();
            CreateModelMap<Comment, AddCommentModel>();
        }

        private void CreateModelMap<From, To>()
        {
            CreateMap<From, To>();
            CreateMap<To, From>();
        }
    }
}
